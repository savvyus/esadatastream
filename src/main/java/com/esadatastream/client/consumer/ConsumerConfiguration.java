package com.esadatastream.client.consumer;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

import com.esadatastream.entity.AvroMessage;
import com.esadatastream.entity.ExtendedAvroMessage;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;

@Configuration
@EnableKafka
public class ConsumerConfiguration {
	
	@Value(value = "${spring.kafka.consumer.bootstrap-servers}")
	private String bootStrapAddress; 
	
	@Value(value = "${spring.kafka.consumer.group-id}")
	private String groupId; 
	
	@Value("${spring.kafka.consumer.key-deserializer}")
	private String keyDeserializer; 
	
	@Value("${spring.kafka.consumer.value-deserializer}")
	private String valueDeserializer; 
	
	@Value("${spring.kafka.properties.schema.registry.url}")
	private String schemaRegistryUrl; 
	
	@Bean
	public Map<String, Object> consumerFactory(){
		Map<String, Object> properties = new HashMap<>(); 
		properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapAddress);
		properties.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
		properties.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaRegistryUrl);
		properties.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, true);
		properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, keyDeserializer);
		properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, EsaMessageDeserializer.class );
		
		return properties;
	}
	
	@Bean 
	public ConsumerFactory<String, AvroMessage> defaultConsumerFactory(){
		return new DefaultKafkaConsumerFactory<>(consumerFactory());
	}
	
	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, AvroMessage> kafkaListenerContainerFactory(){
		ConcurrentKafkaListenerContainerFactory<String, AvroMessage> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(defaultConsumerFactory());
		
		return factory; 
	}
}
