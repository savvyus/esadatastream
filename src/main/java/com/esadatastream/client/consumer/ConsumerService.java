package com.esadatastream.client.consumer;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.esadatastream.entity.AvroMessage;
import com.esadatastream.entity.ExtendedAvroMessage;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ConsumerService {

	// @KafkaListener(topics = "${spring.kafka.topic.name}", groupId = "${spring.kafka.consumer.group-id}")
	// public void listen(ExtendedAvroMessage message) {
	//	 log.info("Received Avro Message in group : {}", message);
	// }
	
	@KafkaListener(	topics = "${spring.kafka.topic.name}", 
					groupId = "${spring.kafka.consumer.group-id}", 
					containerFactory = "kafkaListenerContainerFactory")
	public void listen(AvroMessage message) {
		log.info("Received Raw Avro Message in group : {}", message);
	}
}
