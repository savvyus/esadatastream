package com.esadatastream.client.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.esadatastream.entity.ExtendedAvroMessage;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/publisher")
public class ProducerController {
	@Autowired 
	private ProducerService producerService; 
	
	/* @PostMapping(value = "/send/avro/message")
	public String kafkaAvroMessage(@RequestBody ExtendedAvroMessage message) {
		log.info("Publishing avro messge to Kafka...");
		producerService.sendAvroMessage(message);
		return "AVRO Message Sent SUCCESS";
	}
	
	@PostMapping(value = "/send/message")
	public String kafkaMessage(@RequestParam("msgId") String messageId, @RequestParam("msgDes") String messageDes) {
		producerService.sendMessage(messageId, messageDes);
		return "Message Sent SUCCESS";
	} */
	
	@PostMapping(value = "/send/raw/message")
	public String kafkaRawMessage(@RequestParam("msgId") String messageId, @RequestParam("msgDes") String messageDes) {
		producerService.sendRawMessage(messageId, messageDes);
		return "Raw Message Sent SUCCESS";
	}
}
