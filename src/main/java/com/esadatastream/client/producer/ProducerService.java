package com.esadatastream.client.producer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.esadatastream.entity.AvroMessage;
import com.esadatastream.entity.ExtendedAvroMessage;

import lombok.extern.apachecommons.CommonsLog;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProducerService {
	@Value("${spring.kafka.topic.name}")
	private String topicName;
	
	// @Autowired 
	// private KafkaTemplate<String, ExtendedAvroMessage> kafkaTemplate;
	
	@Autowired 
	private KafkaTemplate<String, AvroMessage> kafkaTemplateRaw;
	
	/*public void sendAvroMessage(ExtendedAvroMessage message) {
		log.info("Sending data to Kafka...");
		ListenableFuture<SendResult<String, ExtendedAvroMessage>> future = kafkaTemplate.send(topicName, message);
		
		future.addCallback(new ListenableFutureCallback<SendResult<String, ExtendedAvroMessage>>()
		{

			@Override
			public void onSuccess(SendResult<String, ExtendedAvroMessage> result) {
				log.info("Sent message=[{}] with offset=[{}]", message, result.getRecordMetadata().offset());
				
			}

			@Override
			public void onFailure(Throwable ex) {
				log.info("Unable to send message=[{}] due to {}", message, ex.getMessage());
			}
			
		});
	}
	
	public void sendMessage(String msgId, String msgDes) {
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
		LocalDateTime now = LocalDateTime.now();
		Integer processDate = Integer.parseInt(dtf.format(now));
		
		sendAvroMessage(new ExtendedAvroMessage(msgId, msgDes, processDate, 0));
	} */
	
	public void sendRawAvroMessage(AvroMessage message) {
		log.info("Sending data to Kafka...");
		ListenableFuture<SendResult<String, AvroMessage>> future = kafkaTemplateRaw.send(topicName, message);
		
		future.addCallback(new ListenableFutureCallback<SendResult<String, AvroMessage>>()
		{

			@Override
			public void onSuccess(SendResult<String, AvroMessage> result) {
				log.info("Sent message=[{}] with offset=[{}]", message, result.getRecordMetadata().offset());
				
			}

			@Override
			public void onFailure(Throwable ex) {
				log.info("Unable to send message=[{}] due to {}", message, ex.getMessage());
			}
			
		});
	}
	
	public void sendRawMessage(String msgId, String msgDes) {
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
		LocalDateTime now = LocalDateTime.now();
		Integer processDate = Integer.parseInt(dtf.format(now));
		
		sendRawAvroMessage(new AvroMessage(msgId, msgDes, processDate, 0));
	}
}
