package com.esadatastream.client.producer;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.esadatastream.entity.ExtendedAvroMessage;

@Repository
public interface ProducerRepository extends CrudRepository<ExtendedAvroMessage, Integer> {

}
