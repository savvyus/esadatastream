package com.esadatastream.client.consumer;

import com.esadatastream.entity.AvroMessage;

public class EsaMessageDeserializer extends SpecificKafkaAvroDeserializer<AvroMessage>{
	  public EsaMessageDeserializer() {
		    super(AvroMessage.class);
		  }
}
