package com.esadatastream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsaDataStreamApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsaDataStreamApplication.class, args);
	}

}
