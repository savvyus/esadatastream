package com.esadatastream.configuration;

import org.springframework.context.annotation.Bean;

import lombok.extern.slf4j.Slf4j;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Slf4j
public class SpringFoxConfiguration {
	@Bean
	public Docket api() {
		log.info("Configure Swagger API info...");
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any())
				.build()
				.apiInfo(esaApiInfo());
	}
	
	
	private ApiInfo esaApiInfo() {
		return new ApiInfoBuilder()
				.title("esaDataStream API Info")
				.version("1.0.0")
				.build();
	}
}
