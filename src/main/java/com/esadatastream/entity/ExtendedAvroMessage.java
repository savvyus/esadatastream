package com.esadatastream.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ESA_MESSAGE")
public class ExtendedAvroMessage extends AvroMessage {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;


	public ExtendedAvroMessage() {
		super();
	}

	public ExtendedAvroMessage(CharSequence messageId, CharSequence messageDescription, Integer processDate,
			Integer status) {
		super(messageId, messageDescription, processDate, status);
	}

	public ExtendedAvroMessage(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "ExtendedAvroMessage [id=" + id + "]";
	}
}
