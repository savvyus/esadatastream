package com.esadatastream.client.producer;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import com.esadatastream.entity.AvroMessage;
import com.esadatastream.entity.ExtendedAvroMessage;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;

@Configuration
public class ProducerConfiguration {
	@Value("${spring.kafka.producer.bootstrap-servers}")
	private String bootStrapAddress;  
	
	@Value("${spring.kafka.producer.key-serializer}")
	private String keySerializer; 
	
	@Value("${spring.kafka.producer.value-serializer}")
	private String valueSerializer;
	
	@Value("${spring.kafka.properties.schema.registry.url}")
	private String schemaRegistryUrl; 
	
	@Bean 
	public Map<String, Object> producerConfig(){
		Map<String, Object> configProperties = new HashMap<>(); 
		configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapAddress);
		configProperties.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaRegistryUrl);
		configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, keySerializer);
		configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, valueSerializer);
		
		return configProperties; 
	}
	
	@Bean
	public ProducerFactory<String, AvroMessage> producerFactory(){
		return new DefaultKafkaProducerFactory<>(producerConfig()); 
	}	
	
	@Bean
	public KafkaTemplate<String, AvroMessage> kafkaTemplate() {
		return new KafkaTemplate<>(producerFactory());
	}
}
